<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Row
{
    protected $header;
    protected $model_class;
    protected $identifiers;
    protected $skip = false;
    protected $after_build_block;
    public $row_array;
    public $model;
    
    public function __construct(
        Header $header,
        Collection $row_array,
        string $model_class,
        Collection $identifiers,
        Closure $after_build_block = null,
        Closure $find_or_build_block = null
    ) {
        $this->header = $header;
        $this->row_array = $row_array;
        $this->model_class = $model_class;
        $this->identifiers = $identifiers;
        $this->after_build_block = $after_build_block;
        $this->find_or_build_block = $find_or_build_block;
        $this->findOrBuildModel();
    }
    
    protected function findOrBuildModel()
    {
        if (is_closure($closure = $this->find_or_build_block)) {
            $closure($this);
        } else {
            if ($model = $this->findModel()) {
                $this->model = $model;
            } else {
                $this->model = $this->buildModel();
            }
        }
        
        $this->setAttributes();
        
        if (is_closure($closure = $this->after_build_block)) {
            $closure($this);
        }
    }
    
    public function setModel(Model $model)
    {
        $this->model = $model;
    }
    
    protected function setAttributes()
    {
        $this->columns()->each(function ($column) {
            $this->setAttribute($this->model, $column, $this->getRowValue($column->name));
        });
    }
    
    protected function setAttribute($model, $column, $csv_value)
    {
        $columnDefinition = $column->definition;
        if ($columnDefinition) {
            if (is_closure($closure = $columnDefinition->to)) {
                $closure($model, $csv_value, $column);
            } else {
                $model->fill([$columnDefinition->name => $csv_value]);
            }
        }
    }
    
    public function columns()
    {
        return $this->header->getColumns();
    }
    
    public function getRowValue($attribute)
    {
        return $this->row_array->get($attribute);
    }
    
    protected function findModel()
    {
        if ($this->identifiers->isNotEmpty() && $this->getIdentifierValues()->isNotEmpty()) {
            $query = $this->model_class::query();
            $queryValues = array_change_key_case($this->getIdentifierValues()->toArray(), CASE_LOWER);
            return $query->where($queryValues)->first();
        }
        return null;
    }
    
    public function buildModel()
    {
        return (new $this->model_class);
    }
    
    protected function getIdentifierValues()
    {
        return $this->row_array->filter(function ($key, $columnName) {
            return $this->identifiers->contains(function ($identifier) use ($columnName) {
                    return strtolower($identifier) === strtolower($columnName);
                }) && !empty($key);
        });
    }
    
    public function skip()
    {
        $this->skip = true;
    }
    
    public function skipped()
    {
        return $this->skip;
    }
    
}