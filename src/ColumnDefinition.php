<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

use mysql_xdevapi\Exception;

class ColumnDefinition
{
    public $name;
    public $options;
    public $as;
    public $to;

    public function __construct($name, $opts = [], $as=null, $to=null)
    {
        $this->name = $name;
        $this->options = collect($opts);
        $this->as = $as;
        $this->to = $to;
    }

    public function attribute()
    {
        if (is_string($this->to)) {
            return $this->to;
        } else {
            return $this->name;
        }
    }

    # Return true if column definition matches the column name passed in.
    public function columnMatch($name)
    {
        if(!$name) return false;
        if($this->as)
        {
            $searchQuery = $this->as;
        }else{
            $searchQuery = $this->name;
        }
        $downcasedColumnName = strtolower($name);
        switch ($searchQuery) {
            case $this->isRegularExpression($searchQuery):
                return preg_match($searchQuery, $name);
            case is_array($searchQuery):
                $pattern = '/'.implode('|', $searchQuery).'/';
                return $this->isRegularExpression($pattern) && preg_match($pattern, $name);
                break;
            case is_string($searchQuery):
                return $downcasedColumnName === strtolower($searchQuery);
                break;
            default:
                throw new Exception("Invalid `as`. Should be String, Regexp or Array - was {$this->as}");
        }
    }

    private function isRegularExpression($string) {
        return @preg_match($string, '') !== FALSE;
    }
}