<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

class Header
{
    protected $column_definitions;
    protected $csv_columns;

    public function __construct($column_definitions, $csv_columns)
    {
        $this->column_definitions = $column_definitions;
        $this->csv_columns = collect($csv_columns);
    }

    public function getColumnDefinitions()
    {
        return $this->column_definitions;
    }

    public function getColumns()
    {
        return $this->csv_columns->filter()->map(function($column){
            return new Column($column, $this->findColumnDefinition($column));
        });
    }

    public function findColumnDefinition($name)
    {
        return $this->column_definitions->filter(function($definition) use ($name){
            return $definition->columnMatch($name);
        })->first();
    }

    public function isValid()
    {
        return $this->missingRequiredColumns()->isEmpty();
    }

    public function missingRequiredColumns()
    {
        $csvColumns = $this->getColumns()->map(function($column){return $column->definition;})->filter()->map(function($defintion){ return $defintion->name; });
        return $this->requiredColumns()->keys()->diff($csvColumns)->values();
    }

    public function requiredColumns()
    {
        return $this->getColumnDefinitions()->filter(function($definition){
           return $definition->options->get('required', false);
        });
    }
}