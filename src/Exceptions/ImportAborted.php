<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter\Exceptions;

use Exception;

class ImportAborted extends Exception
{
}