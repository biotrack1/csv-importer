<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

class Configuration
{
    protected $column_definitions;
    protected $identifiers;
    protected $when_invalid;
    protected $after_save_block;
    protected $after_build_block;
    protected $after_import_block;
    protected $find_or_build_block;
    
    public function __construct()
    {
        $this->column_definitions = collect([]);
        $this->identifiers = collect([]);
        $this->when_invalid = 'abort';
    }
    
    public function setAfterBuildBlock(\Closure $function)
    {
        $this->after_build_block = $function;
    }
    
    public function setFindOrBuildBlock(\Closure $function)
    {
        $this->find_or_build_block = $function;
    }
    
    public function afterBuildBlock()
    {
        return $this->after_build_block;
    }
    
    public function findOrBuildBlock()
    {
        return $this->find_or_build_block;
    }
    
    public function whenInvalid()
    {
        return $this->when_invalid;
    }
    
    public function setAfterSaveBlock(\Closure $function)
    {
        $this->after_save_block = $function;
    }
    
    public function afterSaveBlock()
    {
        return $this->after_save_block;
    }
    
    public function setAfterImportBlock(\Closure $function)
    {
        $this->after_import_block = $function;
    }
    
    public function afterImportBlock()
    {
        return $this->after_import_block;
    }
    
    public function setColumnDefinition($name, $options, $as, $to)
    {
        $this->column_definitions->put($name, new ColumnDefinition($name, $options, $as, $to));
    }
    
    public function setIdentifier($identifier)
    {
        $this->identifiers->push($identifier);
    }
    
    public function getColumnDefinitions()
    {
        return $this->column_definitions;
    }
    
    public function getIdentifiers()
    {
        return $this->identifiers;
    }
}