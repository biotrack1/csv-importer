<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

class AspectsCSVImport extends CSVImporter
{

    public function __construct($stream, $quote_char, $model_class)
    {
       parent::__construct($stream, $quote_char, $model_class);
       $this->fromAspects();
    }

    public function fromAspects()
    {
        $class = $this->modelClass();
        $classInstance = new $class;
        $aspects = $classInstance->builder()->importableAspects();
        $identifiers = $classInstance->builder()->identifierAspects();

        $aspects->each(function($asp){
            $this->setColumn($asp->name, ['required' => $asp->isRequired(), 'type' => $asp->type], ["(?i)^$asp->name(\\[.+\\])?$", "(?i)^{$asp->label()}(\\[.+\\])?$"],
               function($model, $value, $column) use ($asp) { $asp->importFromCsv($model, $value); });
        });

        $identifiers->each(function($asp){
           $this->setIdentifier($asp->name);
        });
    }

}