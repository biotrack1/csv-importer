<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

use Illuminate\Support\Carbon;

/**
 * Class CSVFileImport
 * @package Engeni\CSVImporter
 *
 * Example to use:
 *
 *  $file = '/Users/mcalvo/Sites/dispensary/database/seeds/example_data/strain_type.csv';
 *  $modelClass = 'App\Models\Inventory\StrainType';
 *
 *  $import = Engeni\CSVImporter\CSVFileImport::load($file, $modelClass);
 *  $import->run();
 *
 *  $import->getReport();
 */
class CSVFileImport
{
    
    protected $path;
    protected $model_class;
    protected $encoding;
    protected $quote_char;
    protected $report;
    protected $errors;
    
    public static function load($path, $model_class, $encoding = null, $quote_char = null)
    {
        return new self($path, $model_class, $encoding, $quote_char);
    }
    
    public function __construct($path, $model_class, $encoding = 'windows-1252', $quote_char = '"')
    {
        $this->path = $path;
        $this->model_class = $model_class;
        $this->encoding = $encoding;
        $this->quote_char = $quote_char;
        $this->errors = collect([]);
    }
    
    public function run()
    {
        return $this->process();
    }
    
    private function process()
    {
        if (empty($this->path)) {
            $this->addError('No file to import');
            return $this;
        }
        $stream = fopen($this->path, "r:{$this->encoding}");
        if ($stream) {
            $this->import($stream);
            fclose($stream);
        } else {
            $this->addError('Error trying read the file: ' . $this->path);
        }
        return $this;
    }
    
    private function import($stream)
    {
        $importerClass = $this->importerClass();
        $importer = new $importerClass($stream, $this->quote_char, $this->model_class);
        
        $start_time = Carbon::now();
        
        $this->report = $importer->run();
        
        $end_time = Carbon::now();
        
        $this->report->start_time = $start_time->toDateTimeString();
        $this->report->end_time = $end_time->toDateTimeString();
        $this->report->elapsed_time = $end_time->diff($start_time)->format('%H:%I:%S');
        
        return $this;
    }
    
    public function getReport()
    {
        return $this->report;
    }
    
    private function importerClass()
    {
        if (method_exists($this->modelClass(), 'importerClass')) {
            return $this->modelClass()::importerClass();
        } else {
            return AspectsCSVImport::class;
        }
    }
    
    private function modelClass()
    {
        return $this->model_class;
    }
    
    private function addError($error)
    {
        $this->errors->push($error);
    }
    
    public function getErrors()
    {
        return $this->errors;
    }
    
}