<?php

namespace Engeni\CSVImporter;

use Engeni\ApiTools\Context\AppContextRegister;
use Illuminate\Support\Collection;

class SuccessImportEvent
{
    public $modelClass;
    public $rows;
    public $context;
    
    public function __construct(string $modelClass, Collection $rows)
    {
        $this->modelClass = $modelClass;
        $this->rows = $rows;
        $this->context = app_context();
    }
    
    public function getModelClass(): string
    {
        return $this->modelClass;
    }
    
    public function getRows(): Collection
    {
        return $this->rows;
    }
    
    public function getContext(): AppContextRegister
    {
        return $this->context;
    }
}