<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

use Engeni\CSVImporter\Exceptions\ImportAborted;
use Illuminate\Support\Facades\DB;

class Runner
{
    protected $report;
    protected $rows;
    protected $when_invalid;
    protected $after_save_block;
    protected $after_import_block;

    public static function call($rows, $when_invalid, $after_save_block, $after_import_block)
    {
        $runnerClass = get_called_class();
        return (new $runnerClass($rows, $when_invalid, $after_save_block, $after_import_block))->run();
    }

    public function __construct($rows, $when_invalid, $after_save_block, $after_import_block)
    {
        $this->rows = $rows;
        $this->when_invalid = $when_invalid;
        $this->after_save_block = $after_save_block;
        $this->after_import_block = $after_import_block;
        $this->initializeReport();
    }

    public function run()
    {
        try {
            if ($this->rows->isEmpty()) {
                $this->report->done();
                return $this->report;
            }
            $this->report->inProgress();
            $this->persistRows();
            $this->report->done();
            return $this->report;
        } catch (ImportAborted $e) {
            $this->report->aborted();
            return $this->report;
        }
    }

    protected function persistRows()
    {
        try {
            DB::beginTransaction();
            $this->rows->each(function ($row) {
                $tags = collect([]);

                if ($row->model->exists) {
                    $tags->push('update');
                } else {
                    $tags->push('create');
                }

                if ($row->skipped()) {
                    $tags->push('skip');
                } else {
                    if ($row->model->save()) {
                        $tags->push('success');
                    } else {
                        $tags->push('failure');
                    }
                }

                $this->addToReport($row, $tags);
                if($tags->contains('success'))
                {
                    $this->runAfterSaveCallback($row);
                }
            });

            if($this->report->hasErrors())
            {
                DB::rollBack();
                throw new ImportAborted();
            }else {
                $this->runAfterImportCallback();
                DB::commit();
            }

        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    protected function runAfterSaveCallback($row)
    {
        if(is_closure($closure = $this->after_save_block))
        {
            $closure($row);
        }
    }
    
    protected function runAfterImportCallback()
    {
        if(is_closure($closure = $this->after_import_block))
        {
            $closure($this->rows);
        }
    }
    
    protected function addToReport($row, $tags)
    {
        switch ($tags) {
            case $this->tagsContains($tags, ['create', 'success']):
                $this->report->addCreatedRow($row);
                break;
            case $this->tagsContains($tags, ['create', 'failure']):
                $this->report->addFailedToCreatedRow($row);
                break;
            case $this->tagsContains($tags, ['update', 'success']):
                $this->report->addUpdatedRow($row);
                break;
            case $this->tagsContains($tags, ['update', 'failure']):
                $this->report->addFailedToUpdateRow($row);
                break;
            case $this->tagsContains($tags, ['created', 'skip']):
                $this->report->addCreateSkippedRows($row);
                break;
            case $this->tagsContains($tags, ['update', 'skip']):
                $this->report->addUpdateSkippedRows($row);
                break;
            default:
                new \Exception('Invalid tags');
        }
    }

    private function tagsContains($tags, $contains)
    {
        return $tags->diff($contains)->count() == 0;
    }

    public function getReport()
    {
        return $this->report;
    }
    
    protected function initializeReport()
    {
        $this->report = $this->newReport(['status' => 'pending']);
    }

    private function newReport(array $data)
    {
        return new Report($data);
    }
}