<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

class Report
{
    public $status;
    public $created_rows;
    public $parser_error;
    public $missing_columns = [];
    public $failed_to_created_rows;
    public $updated_rows;
    public $failed_to_update_rows;
    public $create_skipped_rows;
    public $update_skipped_rows;

    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            $this->setData($key, $value);
        }

        $this->created_rows = collect([]);
        $this->failed_to_created_rows = collect([]);
        $this->updated_rows = collect([]);
        $this->failed_to_update_rows = collect([]);
        $this->create_skipped_rows = collect([]);
        $this->update_skipped_rows = collect([]);
    }

    public function hasErrors()
    {
        return $this->failed_to_created_rows->count() > 0 || $this->failed_to_update_rows->count() > 0 || !empty($this->parser_error) || in_array($this->status, ['aborted', 'invalid_header']);
    }

    public function setData($key, $value)
    {
        $this->{$key} = $value;
    }

    public function done()
    {
        $this->status = 'done';
    }

    public function inProgress()
    {
        $this->status = 'in_progress';
    }

    public function aborted()
    {
        $this->status = 'aborted';
    }

    public function addCreatedRow($row)
    {
        $this->created_rows->push($this->getReportRow($row));
    }

    public function addFailedToCreatedRow($row)
    {
        $this->failed_to_created_rows->push($this->getReportRow($row));
    }

    public function addUpdatedRow($row)
    {
        $this->updated_rows->push($this->getReportRow($row));
    }

    public function addFailedToUpdateRow($row)
    {
        $this->failed_to_update_rows->push($this->getReportRow($row));
    }

    public function addCreateSkippedRows($row)
    {
        $this->create_skipped_rows->push($this->getReportRow($row));
    }

    public function addUpdateSkippedRows($row)
    {
        $this->update_skipped_rows->push($this->getReportRow($row));
    }

    private function getReportRow($row)
    {
        return ['model' => $row->model, 'errors' => $row->model->errors(), 'row_array' => $row->row_array];
    }
}