<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

class Column
{
    public $name;
    public $definition;

    public function __construct($name, $definition)
    {
        $this->name = $name;
        $this->definition = $definition;
    }

    public function data()
    {
        if(preg_match('/.*\[(.*)\]/', $this->name, $matches))
        {
            return $matches[1];
        }
    }

}