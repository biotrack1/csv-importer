<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\CSVImporter;

use Illuminate\Support\Collection;

class CSVImporter
{
    protected $stream;
    protected $model_class;
    protected $csvHeader;
    protected $config;
    protected $report;

    public function __construct($stream, $quote_char, $model_class)
    {
        $this->stream = $stream;
        $this->model_class = $model_class;
        $this->config = new Configuration();
        $this->setCsvHeader();
        $this->afterImportBlock(function(Collection $rows) {
            event(new SuccessImportEvent($this->modelClass(), $rows));
        });
    }

    public function afterBuild(\Closure $function)
    {
        return $this->config->setAfterBuildBlock($function);
    }
    
    public function findOrBuildBlock(\Closure $function)
    {
        return $this->config->setFindOrBuildBlock($function);
    }
    
    private function whenInvalid()
    {
        return $this->config->whenInvalid();
    }

    public function afterSaveBlock(\Closure $function)
    {
        return $this->config->setAfterSaveBlock($function);
    }

    public function afterImportBlock(\Closure $function)
    {
        return $this->config->setAfterImportBlock($function);
    }
    
    protected function runnerClass(): string
    {
        return Runner::class;
    }

    public function run()
    {
        $runnerClass = $this->runnerClass();
        
        try {
            if ($this->isValidHeader()) {
                $this->setNewPendingReport();
                $this->report = $runnerClass::call($this->rows(), $this->whenInvalid(), $this->config->afterSaveBlock(), $this->config->afterImportBlock());
            } else {
                $this->setNewInvalidHeaderReport();
            }
            return $this->report;
        }catch(\Throwable $e)
        {
            $this->report = $this->newReport(['status' => 'invalid_csv_file', 'parser_error' => $e->getMessage()]);
            return $this->report;
        }
    }

    private function newReport(array $data)
    {
        return new Report($data);
    }

    private function setNewPendingReport()
    {
        $this->report = $this->newReport(['status' => 'pending']);
    }

    private function setNewInvalidHeaderReport()
    {
        $this->report = $this->newReport(['status' => 'invalid_header', 'missing_columns' => $this->getHeader()->missingRequiredColumns()]);
    }

    public function isValidHeader()
    {
        return $this->getHeader()->isValid();
    }

    public function setColumn($name, $options = [], $as = null, $to = null)
    {
        $this->config->setColumnDefinition($name, $options, $as, $to);
    }

    public function setIdentifier($identifier)
    {
        $this->config->setIdentifier($identifier);
    }

    public function rows()
    {
        $csvRows = $this->getRows();
        return $csvRows->map(function ($row) {
            return $this->buildRow($row);
        });
    }

    private function getHeader()
    {
        return $this->buildHeader();
    }

    private function buildHeader()
    {
        return new Header($this->config->getColumnDefinitions(), $this->getCsvColumns());
    }

    private function getIdentifiers()
    {
        return $this->config->getIdentifiers();
    }

    private function buildRow($row)
    {
        $rowClass = $this->rowClass();
        return new $rowClass($this->getHeader(), $row, $this->modelClass(), $this->getIdentifiers(), $this->config->afterBuildBlock(), $this->config->findOrBuildBlock());
    }
    
    protected function rowClass(): string
    {
        return Row::class;
    }

    public function modelClass()
    {
        return $this->model_class;
    }

    private function getRows()
    {
        try {
            $rows = array();
            $header = $this->csvHeader;
            while ($row = fgetcsv($this->stream)) {
                $rows[] = array_combine($header, $row);
            }
            return r_collect($rows);
        } catch (\Throwable $e) {
            throw new \Exception('CSV parse error. Check headers and rows have equal number of elements');
        }
    }

    private function setCsvHeader()
    {
        $header = $this->removeBomIfPresent(fgets($this->stream));
        $this->csvHeader = str_getcsv($header);
    }

    private function removeBomIfPresent($string)
    {
        //Remove UTF-8 BOM if present.
        if(substr($string, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $string = substr($string, 3);
        return $string;
    }

    function r_collect($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = r_collect($value);
                $array[$key] = $value;
            }
        }
        return collect($array);
    }

    private function getCsvColumns()
    {
        return $this->csvHeader;
    }

    private function getDefaultIdentifiers()
    {
        return collect([]);
    }
}